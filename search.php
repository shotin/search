 <?php
session_start();
	
?>

<!doctype html>
<html lang="en">
  <link>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

   <!-- Custom css -->
    <link href="./include/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./include/parsley/parsley.css">


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

     
    <title>Edves </title>
  </head>
  <!-- <style>
     .eddy {
border-radius: 65px;
background: linear-gradient(145deg, #0090c3, #007aa4);
box-shadow:  1px 1px 2px #007aa4,
             -1px -1px 2px #0095c8;
  </style> -->
  <body>

<nav class="navbar navbar-light bg-light">
  <form class="container">
    <div class="input-group">
      <span class="input-group-text" id="basic-addon1">Search School</span>
      <input type="text" class="form-control" id="live_search" autocomplete="off"
   			placeholder="Search.....">
    </div>
  </form>
</nav>
   <div id="searchresult"></div>

<form action="#">
     <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">demo.edves.net (Edves Academy)</h5>
       
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <p>URL:______</p>
        <p>No of Arms: 3</p>
        <p>School Arms</p>
        <table class="table table-responsive table-striped">
  	  	 		 <thead>
  	  	 		 	<tr>
  	  	 		 		<th>S/N</th>
  	  	 		 		<th>ID</th>
  	  	 		 		<th>NAME</th>
  	  	 		 		<th>ADDRESS</th>
  	  	 		 		<th>PRINCIPAL</th>
  	  	 		 		<th>HEADMASTER</th>
  	  	 		 		<th>PROPRIETOR</th>
                <th>SCHOOL TYPE</th>
                <th>ACTIONS</th>
  	  	 		 	</tr>
  	  	 		 </thead>

  	  	 		 <tbody>
  	  	 		 	<tr>
					    	<td>1</td>
  	  	 		 		<td>1</td>
  	  	 		 		<td>Edves Academy</td>
  	  	 		 		<td>Lagos street Ebutee Metta</td>
  	  	 		 		<td>Mr Kayode</td>
  	  	 		 		<td>Mr Taiwo</td>
  	  	 		 		<td>Mr Victor</td>
  	  	 		 		<td>Secondary</td>
                <td> 
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" checked>
                        <label class="form-check-label" for="flexRadioDefault1">
                          Enabled
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
                        <label class="form-check-label" for="flexRadioDefault2">
                          Disabled
                        </label>
                      </div>
					    	</td>
  	  	 		 	</tr>
  	  	 		 </tbody>
  	  	 	</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</form>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="./include/parsley/parsley.js"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
    	$(document).ready(function(){
    		$('#live_search').keyup(function(){
    				var input = $(this).val(); 
    		        if(input != "") {
    		        	$.ajax({
    		        		url: "livesearch.php",
    		        		method: "POST",
    		        		data:{input:input},

    		        		success:function(data){
    		        			$("#searchresult").html(data);
                      $("#searchresult").css("display","block");
    		        		}
    		        		
    		        	});
    		        }else {
    		        	$("#searchresult").css("display","none");
    		        }
    		})
    	})
    </script>
  </body>
</html>
