<?php
 class Post extends Database {
     //DB Properties
     private $conn;
     private $table = 'student';

     //Post properties
     public $id;
     public $school;
     public $payment;
     public $category_name;
     public $bank;
     public $account_number;
     public $account_name;
     public $account_purpose;

     //Constructor with db
     public function __construct(){
         $this->conn = $this->connect();
     }

    //Get Posts
     public function read() 
     {
         //create query

         $query = 'SELECT id, school, payment, bank, account_number,account_name, account_purpose, created_at 
         FROM ' .  $this->table . '';

    //Prepare Statement
    $stmt = $this->conn->prepare($query);

    //Execute
    $stmt->execute();

    return $stmt;
     }


    public function create() {
        //create query
        $query = 'INSERT INTO  student SET school = :school, payment = :payment, bank = :bank, account_number = :account_number, account_name =  :account_name, account_purpose = :account_purpose';

         //prepare statement
         $stmt = $this->conn->prepare($query);

         // //clean data
         $this->school = htmlspecialchars(strip_tags($this->school));
         $this->payment = htmlspecialchars(strip_tags($this->payment));
         $this->bank = htmlspecialchars(strip_tags($this->bank));
         $this->account_number = htmlspecialchars(strip_tags($this->account_number));
         $this->account_name = htmlspecialchars(strip_tags($this->account_name));
         $this->account_purpose = htmlspecialchars(strip_tags($this->account_purpose));

         //Bind Data
         $stmt->bindParam('school', $this->school);
         $stmt->bindParam('payment', $this->payment);
         $stmt->bindParam('bank', $this->bank);
         $stmt->bindParam('account_number', $this->account_number);
         $stmt->bindParam('account_name', $this->account_name);
         $stmt->bindParam('account_purpose', $this->account_purpose);

         //Execute query
         if ($stmt->execute()) {
           return true;
         }

         //Print error if something goes wrong
         printf("Error: %S. \n", $stmt->error);
         return false;
    }

}
