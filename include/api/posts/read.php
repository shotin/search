<?php
//headers
error_reporting(E_ALL);
ini_set('display_errors', 1);

header('Access-Control-Allow-Origin');
header('Content-Type: application/json');

include_once '../../config/db.php';
include_once '../../model/Posts.php';

//Instantiate DB AND CONNECT
// $database  = new Database();
// $db  = $database->connect();

//Instantaite blog post object
$post = new Post();

//Blog post query
$result = $post->read();

//get row count
$num = $result->rowCount();

//Check if any Posts
if ($num > 0) {
   //Post array
   $posts_arr =  array();
   $posts_arr['']  = array();
   while($row = $result->fetch(PDO::FETCH_ASSOC)) { 
      extract($row);
      $post_item  = array(
        'id'                 =>  $id,
        'school'             =>  $school,
        'payment'            =>  $payment,
        'bank'               =>  $bank,
        'account_number'     =>  $account_number,
        'account_name'       =>  $account_name,
        'account_purpose'    =>  $account_purpose
      );

      //Push to 'data
      array_push($posts_arr['data'], $post_item);
   }

   //Turn to json and output
   echo json_encode($posts_arr);
} else {
    //No Post
    echo json_encode(
        array('message' => 'No posts found')
    );
}