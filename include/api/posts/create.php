<?php
//headers
header('Access-Control-Allow-Origin');
header('Content-Type: application/json');
header('Access-Control-Allow-Method: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/db.php';
include_once '../../model/Posts.php';

//Instantiate DB AND CONNECT
// $database  = new Database();
// $db  = $database->connect();

//Instantaite blog post object
$post = new Post();

//Get raw posted data
$data = json_decode(file_get_contents('php://input'));

// school, payment, bank, number, account_name, account_purpose
$post->school = $data->school;
$post->payment  = $data->payment;
$post->bank = $data->bank;
$post->account_number  = $data->account_number;
$post->account_name  = $data->account_name;
$post->account_purpose  = $data->account_purpose;

//create post
if ($post->create()) {
    echo json_encode(
        array('message'  => 'Post Created')
    );
}else {
    echo json_encode(
        array('message'  => 'Post Not Created')
    );
}
