<?php
session_start();
	
include ("include/config/db.php");
include ("include/model/post.php");

$userloggedin = null; //id, school, payment, bank, account_number, account_name, account_purpose


// if (isset($_SESSION['userloggedin'])) {
//   $userloggedin = $_SESSION['userloggedin'];
// } else {
//   header("Location: Index.php");
// }

$result = false;
$post = new Post();

if (isset($_POST['submit'])) {

  $school = $_POST['school'];
  $payment =$_POST['payment'];
  $bank =$_POST['bank'];
  $account_number = $_POST['account_number'];
  $account_purpose =   $_POST['account_purpose'];
  $account_name =  $_POST['account_name'];

  $result = $post->verifyStudent($school, $payment, $bank, $account_number, $account_name, $account_purpose);
}
?>

<!doctype html>
<html lang="en">
  <link>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

   <!-- Custom css -->
    <link href="./include/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./include/parsley/parsley.css">


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

     
    <title>Edves </title>
  </head>
  <style>
     .eddy {
border-radius: 65px;
background: linear-gradient(145deg, #0090c3, #007aa4);
box-shadow:  1px 1px 2px #007aa4,
             -1px -1px 2px #0095c8;
  </style>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary">
        <div class="container-fluid">
          <a class="navbar-brand text-white font-weight-bold" href="#"><h4>Edves</h4></a>
        </div>
      </nav>

    <div class="container">
          <div class="row" style="margin-top: 40px;">
                <div class="col-sm-12 col-lg-5 mx-auto edv p-3">
                     <div class="eddy p-5">
                          <h5 class="text-center text-white text-uppercase">Edves International Academy</h5>
                          <div class="card-body p-1">
                            <form class="mt-5" id="form" method="POST" data-parsley-validate="" action="">
                                <div class="mb-3">
                                    <label for="exampleInputPassword1" class="form-label">School</label>
                                    <select class="form-select" required="" name="school" aria-label="Default select example">
                                        <option name="school" value="Police Collage">Police Collage</option>
                                        <option name="school" value="Government Secondary Schools">Government Secondary Schools</option>
                                        <option name="school" value="His Grace Basic School">His Grace Basic School</option>
                                      </select>
                                </div>

                                <div class="mb-3">
                                    <label for="exampleInputPassword1" class="form-label">Payment Gateway</label>
                                    <select class="form-select" required="" name="payment" aria-label="Default select example">
                                        <option name="payment" value="FLUTTERWAVE">FLUTTERWAVE</option>
                                        <option name="payment" value="INTERSWITCH">INTERSWITCH</option>
                                      </select>
                                </div>
                               
                                <div class="mb-3">
                                    <label for="exampleInputPassword1" class="form-label">Bank</label>
                                    <select class="form-select" required="" name="bank" aria-label="Default select example">
                                        <option name="bank" value="Access Bank">Access Bank</option>
                                        <option name="bank" value="GTCO">GTCO</option>
                                        <option name="bank" value="Zenith">Zenith</option>
                                      </select>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label for="exampleInputPassword1" class="form-label">Account Number</label>
                                        <input type="number" required="" name="account_number" min="1" class="form-control"> 
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-light confirm">Verify Account</button>
                                    </div>
                                </div><br>

                                <div class="mb-3">
                                    <label for="exampleFormControlInput1" class="form-label">Account Name</label>
                                    <input type="text" required="" name="account_name" value="John Doe" class="form-control" readonly=""> 
                                </div>

                                <div class="mb-3">
                                    <label for="exampleFormControlInput1" class="form-label">Purpose of Account</label>
                                    <textarea class="form-control" required="" name="account_purpose" placeholder="Max 140 characters" maxlength="140" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                                
                                <button type="submit" name="submit" class="btn btn-light w-100 mt-2">Submit</button>
                            </form>
                          </div>
                     </div>
                </div>
          </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script src="./include/parsley/parsley.js"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>

<script>
  $('#form').parsley();
  </script>

<script>
    if (window.history.replaceState ) {
        window.history.replaceState (null, null, window.location.href);
    }
</script>